import Vue, { VNode } from 'vue'

export default Vue.extend({
  name: 'dynamic-marquee-element',
  props: {
    progress: Number,
    direction: {
      default: 'column',
      validator(val) {
        return ['column', 'row'].includes(val)
      },
    },
    reverse: {
      default: false,
    },
    wrapperDirection: {
      default: 'ltr',
      validator(val) {
        return ['ltr', 'rtl', ''].includes(val)
      },
    },
  },

  computed: {
    axis() {
      switch (this.direction) {
        case 'row':
          return 'X'
        case 'column':
        default:
          return 'Y'
      }
    },

    widthOrHeight() {
      if (this.direction === 'row') {
        return 'height'
      }
      return 'width'
    },

    initialPosition() {
      if (this.direction === 'row') {
        if (
          (this.wrapperDirection === 'ltr' && !this.reverse) ||
          (this.wrapperDirection === 'rtl' && this.reverse)
        ) {
          return { right: '100%' }
        } else {
          return { left: '100%' }
        }
      } else {
        if (this.reverse) {
          return { top: '100%' }
        } else {
          return { bottom: '100%' }
        }
      }
    },
    transform() {
      return {
        transform: `translate${this.axis}(${this.progress}px)`,
      }
    },
  },
  render(h) {
    return h(
      'div',
      {
        ref: 'marqueeElement',
        style: {
          position: 'absolute',
          [this.widthOrHeight]: '100%',
          ...this.initialPosition,
          ...this.transform,
        },
      },
      this.$slots.default
    )
  },
})
